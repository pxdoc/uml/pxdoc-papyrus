package org.pragmaticmodeling.pxdoc.runtime.ui.papyrus;

import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.uml2.uml.Element;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;

public class DefaultPapyrusModelProvider implements IModelProvider {

	@Override
	public Object adaptSelection(Object selection) {
		if (selection instanceof EObjectTreeElement) {
			EObjectTreeElement ete = (EObjectTreeElement)selection;
			return ete.getEObject();
		} else if (selection instanceof Element) {
			return selection;
		}
		return selection;
	}
	
}
