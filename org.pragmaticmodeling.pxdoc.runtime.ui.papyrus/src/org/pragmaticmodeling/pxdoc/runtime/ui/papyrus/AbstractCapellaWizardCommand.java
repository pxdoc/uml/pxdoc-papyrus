//package org.pragmaticmodeling.pxdoc.runtime.ui.capella;
//
//
//
//import org.eclipse.jface.window.Window;
//import org.eclipse.swt.widgets.Display;
//import org.polarsys.capella.common.ef.command.AbstractReadWriteCommand;
//
//import com.google.inject.Injector;
//
//import fr.pragmaticmodeling.pxdoc.runtime.DefaultModelProvider;
//import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
//import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
//import fr.pragmaticmodeling.pxdoc.runtime.IResourceProvider;
//import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
//import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
//import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;
//import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.internal.PxDocWizardDialog;
//import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.jobs.PxDocGeneratorJob;
//import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.IPxDocWizard;
//
//public abstract class AbstractCapellaWizardCommand extends AbstractReadWriteCommand implements IPxDocCommand {
//
//	
//	IStylesheetsRegistry stylesheetsRegistry;
//	
//	
//	private IModelProvider modelProvider;
//	
//	IResourceProvider provider;
//	private Object selection;
//	
//	private AbstractPxUiPlugin uiActivator;
//
//	private Injector injector;
//
//	private IPxDocGenerator pxDocGenerator;
//
//	public AbstractCapellaWizardCommand(final Object selection) {
//		super();
//		this.selection = selection;
//	}
//
//	@Override
//	public void run() {
//		// Wizard creation
//		IPxDocWizard wizard = createWizard(selection);
//		wizard.setStylesheetsRegistry(stylesheetsRegistry);
//
//		// Instantiates the wizard container with the wizard and opens it
//		PxDocWizardDialog dialog = new PxDocWizardDialog(Display.getDefault().getActiveShell(), wizard);
//		dialog.create();
//		dialog.open();
//		if (dialog.getReturnCode() == Window.CANCEL)
//			return;
//
//		final IPxDocGenerator pxdocGenerator = wizard.getPxDocGenerator();
//		beforeGenerate(pxdocGenerator);
//		PxDocGeneratorJob pxdocJob = new PxDocGeneratorJob("", pxdocGenerator, provider);
//		pxdocJob.setUser(true);
//		pxdocJob.schedule();
//	}
//
//	private IPxDocWizard createWizard(Object selection) {
//		IPxDocWizard pxDocWizard = injector.getInstance(IPxDocWizard.class);
//		pxDocWizard.setSelection(selection);
//		pxDocWizard.setPxDocGenerator(getPxDocGenerator());
//		pxDocWizard.setModelProvider(getModelProvider());
//		pxDocWizard.setUiActivator(uiActivator);
//		return pxDocWizard;
//	}
//	
//	protected void beforeGenerate(IPxDocGenerator pxdocGenerator) {
//
//	}
//
//	protected void initLauncher(PxDocLauncher launcher) {
//		launcher.setPluginId(uiActivator.getPxDocGeneratorPluginId());
//		
//	}
//
//	//public abstract AbstractPxDocGenerator createGenerator();
//	
//	public IPxDocGenerator createGenerator() {
//		IPxDocGenerator pxDocGenerator = injector.getInstance(IPxDocGenerator.class);
//		initLauncher(pxDocGenerator.getLauncher());
//		return pxDocGenerator;
//	}
//
//	public IModelProvider getModelProvider() {
//		if (modelProvider == null) {
//			modelProvider = new DefaultModelProvider();
//		}
//		return modelProvider;
//	}
//	
//	@Override
//	public void setStylesheetRegistry(IStylesheetsRegistry reg) {
//		this.stylesheetsRegistry = reg;
//	}
//	
//	@Override
//	public void setResourceProvider(IResourceProvider provider) {
//		this.provider = provider;
//	}
//	
//	@Override
//	public void initialize(Injector injector) {
//		this.injector = injector;
//		getPxDocGenerator().initialize(injector);
//	}
//
//	@Override
//	public IPxDocGenerator getPxDocGenerator() {
//		if (pxDocGenerator == null) {
//			pxDocGenerator = createGenerator();
//		}
//		return pxDocGenerator;
//	}
//	
//	@Override
//	public void setUiActivator(AbstractPxUiPlugin uiActivator) {
//		this.uiActivator = uiActivator;
//	}
//}
