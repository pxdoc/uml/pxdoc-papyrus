package org.pragmaticmodeling.pxdoc.runtime.ui.papyrus;

import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;

public class AbstractPapyrusPropertyTester extends DefaultPropertyTester {

	public AbstractPapyrusPropertyTester(Class<?> clazz) {
		super(clazz);
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (receiver instanceof EObjectTreeElement) {
			EObjectTreeElement ete = (EObjectTreeElement)receiver;
			return super.test(ete.getEObject(), property, args, expectedValue);
		}
		return super.test(receiver, property, args, expectedValue);
	}

}
