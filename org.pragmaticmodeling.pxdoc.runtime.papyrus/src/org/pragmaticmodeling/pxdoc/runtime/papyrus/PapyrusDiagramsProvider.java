package org.pragmaticmodeling.pxdoc.runtime.papyrus;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;
import org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramQueryBuilderImpl;

public class PapyrusDiagramsProvider implements IDiagramProvider<IDiagramQueryBuilder> {

	private IDiagramsQueryRunner runner;
	
	public PapyrusDiagramsProvider() {
		super();
		this.runner = new PapyrusDiagramsQueryRunner();
	}

	@Override
	public IDiagramQueryBuilder createDiagramsQuery(Object namespace) {
		return createQuery(namespace);
	}

	@Override
	public IDiagramQueryBuilder createQuery(Object namespace) {
		return new DiagramQueryBuilderImpl(namespace, runner);
	}

}
