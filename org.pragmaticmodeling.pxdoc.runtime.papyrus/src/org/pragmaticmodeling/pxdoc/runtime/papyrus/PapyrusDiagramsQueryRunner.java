package org.pragmaticmodeling.pxdoc.runtime.papyrus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.image.ImageFileFormat;
import org.eclipse.gmf.runtime.diagram.ui.render.util.CopyToImageUtil;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.uml.tools.providers.SemanticUMLContentProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Activity;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.DiagramsFactory;
import org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQuery;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;

public class PapyrusDiagramsQueryRunner implements IDiagramsQueryRunner {

	@Override
	public List<Diagram> execute(IDiagramQueryBuilder queryBuilder) {
		IDiagramQuery query = queryBuilder.getQuery();
		final List<Diagram> diagrams = new ArrayList<Diagram>();
		final EObject namespace = (EObject) query.getNamespace();
		final String kind = query.getType();
		final String name = query.getName();
		final String nameStartsWith = query.getNameStartsWith();
		final boolean ignoreCase = query.ignoreCase();
		final boolean searchNested = query.searchNested();
		final boolean notEmpty = query.notEmpty();
		final CopyToImageUtil copyImageUtil = new CopyToImageUtil();

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					SemanticUMLContentProvider cp = new SemanticUMLContentProvider(
							namespace.eResource().getResourceSet());
					Set<org.eclipse.gmf.runtime.notation.Diagram> gmfDiagrams = new HashSet<org.eclipse.gmf.runtime.notation.Diagram>();
					visitElement(namespace, cp, gmfDiagrams, searchNested);
					DiagramsFactory factory = DiagramsPackage.eINSTANCE.getDiagramsFactory();
					for (org.eclipse.gmf.runtime.notation.Diagram diagram : gmfDiagrams) {
						if (matches(diagram, name, kind, nameStartsWith, ignoreCase, notEmpty)) {
							String id = diagram.eResource().getURIFragment(diagram);
							File tempFile = File.createTempFile(id, ".png");
							IPath path = new Path(tempFile.getAbsolutePath());
							copyImageUtil.copyToImage(diagram, path, ImageFileFormat.PNG, new NullProgressMonitor(),
									PreferencesHint.USE_DEFAULTS);
							Image image = new Image(Display.getDefault(), tempFile.getAbsolutePath());
							Diagram pxdocDiagram = factory.createDiagram();
							pxdocDiagram.setName(diagram.getName());
							pxdocDiagram.setPath(tempFile.getAbsolutePath());
							pxdocDiagram.setWidth(image.getBounds().width);
							pxdocDiagram.setHeight(image.getBounds().height);
//							pxdocDiagram.setDocumentation(getDiagramDescription(diagram));
							pxdocDiagram.setId(id);
							pxdocDiagram.setDiagramObject(diagram);
							diagrams.add(pxdocDiagram);
							image.dispose();
							tempFile.deleteOnExit();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			private void visitElement(Object namespace, SemanticUMLContentProvider cp,
					Set<org.eclipse.gmf.runtime.notation.Diagram> gmfDiagrams, boolean searchNested) {
				if (namespace instanceof Activity) {
					// look in owner
					Activity activity = (Activity) namespace;
//					Element owner = activity.getOwner();
//					visitElementChildsForActivity(activity, cp, gmfDiagrams);
					visitElementChilds(activity.getOwner(), activity, cp, gmfDiagrams, false);
				}
				visitElementChilds(namespace, namespace, cp, gmfDiagrams, searchNested);

			}

			private void visitElementChilds(Object owner, Object semantic, SemanticUMLContentProvider cp,
					Set<org.eclipse.gmf.runtime.notation.Diagram> gmfDiagrams, boolean searchNested) {
				for (Object child : cp.getChildren(owner)) {
					Object adapted = child;
					if (child instanceof EObjectTreeElement) {
						adapted = ((EObjectTreeElement) child).getEObject();
					}
					if (adapted instanceof org.eclipse.gmf.runtime.notation.Diagram) {
						org.eclipse.gmf.runtime.notation.Diagram diagram = (org.eclipse.gmf.runtime.notation.Diagram) adapted;
						if (diagram.getElement().equals(semantic)) {
							gmfDiagrams.add(diagram);
						}
					}
					if (searchNested) {
						visitElement(child, cp, gmfDiagrams, searchNested);
					}
				}
			}

//			private void visitElementChildsForActivity(Activity activity, SemanticUMLContentProvider cp,
//					List<org.eclipse.gmf.runtime.notation.Diagram> gmfDiagrams) {
//				for (Object child : cp.getChildren(activity.getOwner())) {
//					Object adapted = child;
//					if (child instanceof EObjectTreeElement) {
//						adapted = ((EObjectTreeElement) child).getEObject();
//					}
//					if (adapted instanceof org.eclipse.gmf.runtime.notation.Diagram) {
//						org.eclipse.gmf.runtime.notation.Diagram diagram = (org.eclipse.gmf.runtime.notation.Diagram) adapted;
//						if (diagram.getElement().equals(activity)) {
//							gmfDiagrams.add(diagram);
//						}
//					}
//				}
//			}

		});

		return diagrams;
	}

	private boolean matches(org.eclipse.gmf.runtime.notation.Diagram diagram, String name, String kind,
			String nameStartsWith, boolean ignoreCase, boolean notEmpty) {
		String currentKind = diagram.getType();
		String currentName = diagram.getName();
		if (name != null) {
			if (ignoreCase && !name.equalsIgnoreCase(currentName))
				return false;
			if (!ignoreCase && !name.equals(currentName))
				return false;
		}
		if (kind != null && !kind.equalsIgnoreCase(currentKind))
			return false;
		if (nameStartsWith != null) {
			if (ignoreCase && !currentName.toLowerCase().startsWith(nameStartsWith.toLowerCase()))
				return false;
			if (!ignoreCase && !currentName.startsWith(nameStartsWith))
				return false;
		}
		if (notEmpty) {
			if (isEmpty(diagram))
				return false;
		}
		return true;
	}

	private boolean isEmpty(org.eclipse.gmf.runtime.notation.Diagram diagram) {
		return diagram.getChildren().isEmpty();
	}

}
