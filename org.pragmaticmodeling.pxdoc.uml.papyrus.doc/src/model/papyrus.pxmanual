document "pxDoc - Papyrus Integration" {
	@html 'This enabler provides full pxDoc capabilities to the <a href="https://www.eclipse.org/papyrus/" target="_blank">Papyrus UML2 modeling environment</a> .'
		
	section "Installation" { 
		@html '	<p><b>First make sure that pxDoc is already installed</b> (if this is not the case: <a href="https://pxdoc.fr/index.php/download" target="_blank">install pxDoc</a>).</p>
							<p> To install the enabler:</p>
				<ul>
				<li>In Eclipse, Choose <b><i>Help -> Install New Software...</i></b> from the menu bar and <b><i>Add....</i></b></li>
				<li>Insert the <a href="http://www.pxdoc.fr/updates/pxdoc-papyrus/releases">http://www.pxdoc.fr/updates/pxdoc-papyrus/releases</a> URL.</li>
				<li>Select the <i>pxDoc Papyrus Integration</i> from the category <i>pxDoc Integrations</i> and complete the wizard by clicking the <b><i>Next</i></b> button until you can click <b><i>Finish.</i></b></li>
				<li>After a quick download and a restart of Eclipse, the pxDoc Papyrus enabler is ready to use.</li>
				</ul>
				<p>You can also test the nightly built: <a href="http://www.pxdoc.fr/updates/pxdoc-papyrus/nightly">update site nightly built</a>'

	
	}
	
	section "Quick start" {
		
		section "Browse the examples" {
			
			@html '<p>Once installed, you can import the <b>UML Generic</b> example dedicated to Papyrus, and many other pxDoc sample projects.:</p>	
			<p>Then <b><i>File -> New -> Example...</i></b></p>'
			
			image "images/papyrus-enabler/new-example.png"
			@html '<p><b>Note:</b> The sample generator is included as plugin in the enabler, so you can launch the generation directly from a model, without installing the examples.</p>'
					
		}

	
		section "Initialize a new pxDoc generator" {
			@html "<p>See <a href='https://pxdoc.fr/index.php/manageproject#wizardCreation'>Project creation</a> for more details on pxDoc projects creation.</p>
			<p>First create an empty pxDoc project, with <b><i>File -> New -> Project...</i></b> and select the <i>pxDoc Project</i> wizard.</p>
			<p>In the second page of the wizard (see screenshot below), select the <i>Papyrus</i> enabler. If you do not want the common libraries (common modules and associated means) to be automatically imported in your project, un-check the checkbox.<p>
			 <p>If you do not have a Word stylesheet (.dotx file) available, you can select the one deployed in the EMF Generic Generator</p>"
			image "images/papyrus-enabler/new-project.png" width:700
			@html "<p>Click on finish.</p>
				<p> First, the pxDoc project itself is created and the <i>.pxdoc</i> file opens. You may notice errors in this project during few seconds:</p>"
			image "images/papyrus-enabler/new-project-2.png" width:700
			@html "<p>These errors are automatically solved with the second step of the project creation:</p>"
			image "images/papyrus-enabler/new-project-3.png" width:700
			@html '<p>If you included the Common Libraries, your last step is to properly bind the <a href="https://pxdoc.fr/index.php/documentconcepts#applyStyles" target="_blank">variable styles</a> from the modules with the chosen stylesheet: use CTRL-SPACE to get all available styles (from the stylesheet) suggested.</p>'
			image "images/papyrus-enabler/new-project-style-binding.PNG" width:700
			
			@html class:note'<p>Your pxDoc generator is ready to use with a Papyrus model.</p>
				<p><b> Do not hesitate to go through the provided examples and the <a href="http://www.pragmatic-modeling.org/index.php?option=com_content&view=article&id=17&Itemid=132" target="_blank"><i>Common Libraries</i> documentation</a> to learn quickly how to rule with pxDoc!</b></p>'												
		}
	
		section launcherWizard "Launching the generation"{ 
			@html '<p>To test or launch your generator,:</p>
			<ol><li> select <i>Debug As</i> or <i>Run As -> Eclipse Application</i>.</li>' 
			class:note'In Debug mode, the modifications made to your generator are automatically taken into account, so you just need to re-launch the generation in the 2nd Eclipse to see the result.'
			@html'<li>Import your source model, data... in this 2nd Eclipse</li>
			<li>To enable the pxDoc menu, right click on a relevant object of this source (according to the command object defined in the <span class="pxDocKeyword">root template</span> of your Generator).</li>
			</ol>
			<p>The launching wizard will pop up</p>.'
			@html class:note 'Refer to <a href="https://pxdoc.fr/index.php/5-min-tutorial" target = "_blank">5min tutorial</a> and to the <a href="https://pxdoc.fr/index.php/15-min-tutorial-generate-a-document-from-a-simple-model" target="_blank">15 min tutorial</a> to get a better view on generation launching!'
			
				
			} // end of launcher wizard
					
	}
	section useCommonLibs "Use the pxDoc common libs in your generator" {
		@html'If you did not uncheck the <i>Use pxDoc libs</i> in the project creation wizard, you can take benefits of the <a href="http://www.pragmatic-modeling.org/index.php?option=com_content&view=article&id=17&Itemid=132" arget="_blank">pxDoc Common Libraries</a>.<p>
		<p>With these libraries, your generator is ready to:</p>
		<ul><li>Use the Papyrus Description Provider to benefits from  CommonServices and EmfServices to include the description of an element (incl. its icon, attributes...) in several formats (table, bullet point)</li>
		<li>Query and include Papyrus diagrams</li>
		<li><b>Use the templates of the <i>UMLCommon</i> Module dedicated to each type of element (class, interface, component...)</b>. You can import the library in your workspace from the Plug-ins view: /org.pragmaticmodeling.pxdoc.uml.common.lib </li></ul>
		<p></p>
		<p><i>If you unchecked the <i>Use pxDoc libs</i> checkbox, you can of course import manually the Modules and plugins. Do not forget to add the relevant dependencies to the MANIFEST.MF of the project.</i></p> '
	
		section queryDiagrams "Query and include Papyrus Diagrams" {
			 
		@html'<p>Below are some sample queries you can use:</p>'
			@pxdoc " 
				// diagrams owned by the 'model' object
			var List<Diagram> ownedDiagrams = queryDiagrams(model).execute;

			// all diagrams owned directly and indirectly by the 'model' object
			var List<Diagram> allOwnedDiagrams = queryDiagrams(model).searchNested.execute;

			// diagrams owned by the 'model' object, with some content
			var List<Diagram> ownedNonEmptyDiagrams = queryDiagrams(model).notEmpty.execute;

			// look for a diagram owned by 'model' and named 'A diagram name'
			var List<Diagram> diagramsByName = queryDiagrams(model).name('A diagram name').execute;

			// look for a diagram owned by 'model' with name staring with 'ABC'
			var List<Diagram> diagramsByNameStartingWith = queryDiagrams(model).nameStartsWith('ABC').execute;

			// diagrams by type
			var List<Diagram> diagramsByType_CDB = queryDiagrams(model).type('CDB').execute;

			// diagrams with type 'CDB' and name starting with 'MyString'
			var List<Diagram> cdbStartingWithMyString = queryDiagrams(model).type('CDB').nameStartsWith('MyString').
				execute;"

			@html "<<p>To include the diagrams:</>"
			@pxdoc "
			// Your defined query
			var List<Diagram> myDiagramQueryName = queryDiagrams(model).execute;

			// Use the template from the DiagramsService module
			// (as you can see, you can apply a template directly on a List of objects: no need to define explicitly a loop!)
			apply includeDiagram(myDiagramQueryName)

			// OR insert the diagram image by yourself...
			for (diagram : myDiagrams) {
				
				// use the pxdoc 'image' keyword with diagram image file path stored in the 'path' diagram attribute.
				image path:diagram.path  
				// add a title with a style
				#Figure keepWithNext align:center {
					diagram.name
				}
				// include diagram documentation if any
				if (!diagram.documentation.empty) {
					#BodyText {diagram.documentation}
				}
			}"
  
		} // end of Query and include diagrams
		
		
		} // end of Use pxDoc common libs

		section GitLab "Find the project on GitLab" {
			@html'You can find the source project on GitLab: <a href="https://gitlab.com/pxdoc/pxdoc-papyrus" target = "_blank">https://gitlab.com/pxdoc/pxdoc-papyrus</a>.'
		}
		section pxDoc "Get support on pxDoc" {
			@html '<p>Read the documentation on <a href="https://www.pxdoc.fr/index.php/documentation" target="_blank">www.pxdoc.fr</a>.</p>'
		}

}