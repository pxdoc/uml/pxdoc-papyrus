package org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui;

import com.google.inject.Inject;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocLauncherHelper;

public class PapyrusGeneratorCommand extends AbstractPapyrusGeneratorCommand {

	@Inject
	IStylesheetsRegistry stylesheetRegistry;

	@Override
	protected void initLauncher(PxDocLauncher launcher) {
		// TODO Auto-generated method stub
		super.initLauncher(launcher);
		IPxDocGenerator pxDocGenerator = getPxDocGenerator();
		pxDocGenerator.setStylesheet(stylesheetRegistry.getStylesheet("PapyrusStylesheet"));
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "Emphasis", "Quote");
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "BulletList", "ListParagraph");
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "Title", "Title");
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "BodyText", "Normal");
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "Normal", "Normal");
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "SubTitle", "Subtitle");
		PxDocLauncherHelper.addStyleBinding(pxDocGenerator, "Figure", "Normal");
	}
}
