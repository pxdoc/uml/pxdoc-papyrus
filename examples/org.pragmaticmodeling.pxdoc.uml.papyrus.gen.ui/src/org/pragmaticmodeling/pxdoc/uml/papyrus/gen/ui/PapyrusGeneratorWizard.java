package org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui;

/**
 * Wizard class
 */
public class PapyrusGeneratorWizard extends AbstractPapyrusGeneratorWizard {

	/**
	 * Return a unique ID for an object in order to persist the generator settings for this object
	 */
	@Override
	public String getModelIdentifier(Object model) {
		// TODO Auto-generated method stub
		return super.getModelIdentifier(model);
	}
	
	/**
	 * Add the custom page to the generation launching wizard
	 */
	
	@Override
	public void addPages() {
		super.addPages();
		addPage(new PapyrusGeneratorOptionsPage());
	}
	
	
}
	 
	
