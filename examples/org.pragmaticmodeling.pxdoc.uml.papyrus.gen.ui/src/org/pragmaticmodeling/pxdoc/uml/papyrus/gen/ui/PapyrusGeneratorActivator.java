package org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui;
		
import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.pragmaticmodeling.pxdoc.uml.papyrus.gen.PapyrusGeneratorModule;

import com.google.inject.Guice;
import com.google.inject.Module;

import fr.pragmaticmodeling.common.guice.Modules2;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.AbstractPxGuiceAwarePlugin;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxGuiceAwareUiPlugin;
		
/**
* The activator class controls the plug-in life cycle
*/
public class PapyrusGeneratorActivator extends AbstractPxGuiceAwareUiPlugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui";
	public static final String GENERATOR_PLUGIN_ID = "org.pragmaticmodeling.pxdoc.uml.papyrus.gen";
	
	private static Logger logger = Logger.getLogger(AbstractPxGuiceAwarePlugin.class);
	
	private static PapyrusGeneratorActivator instance;
	
			
	/**
	 * The constructor
	 */
	public PapyrusGeneratorActivator() {
		super();
		instance = this;
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		try {
			setInjector(Guice.createInjector(Modules2.mixin((Module)new PapyrusGeneratorUiModule(this), new PapyrusGeneratorModule())));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
	}
	
	protected String getPluginId() {
		return PLUGIN_ID;	
	}
	
	public String getPxDocGeneratorPluginId() {
		return GENERATOR_PLUGIN_ID;	
	}
	
	public static PapyrusGeneratorActivator getInstance() {
		return instance;
	}
		
}