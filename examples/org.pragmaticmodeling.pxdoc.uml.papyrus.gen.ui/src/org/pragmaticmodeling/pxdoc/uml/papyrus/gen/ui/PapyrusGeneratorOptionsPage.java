package org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui;

import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.pragmaticmodeling.pxdoc.emf.lib.DocKind;

import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.IPxDocWizardPage;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.IPxDocWizard;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocLauncherHelper;

/**
 * Custom wizard page defined to set properties: title, company name, generation strategy, etc.
 *
 */
public class PapyrusGeneratorOptionsPage extends WizardPage implements IPxDocWizardPage {

	private static final String SETTINGS_ID = "PapyrusGeneratorOptionsPage_Settings";

	private static final String GENERATION_STRATEGY = "generationStrategy";
	private static final String SOCIETY = "society";

	private static final String SUGGEST_IMPROVEMENTS = "suggestImprovements";

	private static final String DOCUMENT_TITLE = "title";

	private static final String DOCUMENT_VERSION = "version";

	private DocKind generationStrategy = DocKind.form;

	private Button genStyleTextualChoice;

	private Button genStyleTablesChoice;

	private Text societyName;
	private boolean suggestImprovements;

	private Button improveButton;

	private Text titleText;

	private Text versionText;

	public PapyrusGeneratorOptionsPage() {
		super("UML Generic Generator Options");
		setTitle("UML Generic Generator Options");
		setDescription("Illustrates how to implement a custom wizard page with generation parameters.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout gridLayout = new GridLayout(3, false);
		composite.setLayout(gridLayout);
		FontData fontData = composite.getFont().getFontData()[0];
		Font font = new Font(parent.getDisplay(), new FontData(fontData.getName(), fontData
		    .getHeight(), SWT.ITALIC));
		Label generationStyleLabel = new Label(composite, SWT.WRAP);
		generationStyleLabel.setText("Generation Style :");
		Group generationStyle = new Group(composite, SWT.NONE| SWT.FLAT | SWT.SHADOW_NONE);
		generationStyle.setLayout(new RowLayout(SWT.VERTICAL));

		genStyleTextualChoice = new Button(generationStyle, SWT.RADIO);
		genStyleTextualChoice.setText("Text based");
		genStyleTextualChoice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setGenerationStyle(DocKind.form);
			}
		});

		genStyleTablesChoice = new Button(generationStyle, SWT.RADIO);
		genStyleTablesChoice.setText("Tables based");
		genStyleTablesChoice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setGenerationStyle(DocKind.table);
			}
		});
		Label strategyLabel = new Label(composite, SWT.WRAP);
		strategyLabel.setText(
				"Choose between two dummy provided ways of generating Capella objects (textual or based on tables).\n");
		strategyLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		setFont(font, strategyLabel);

		// Document title
		Label titleLabel = new Label(composite, SWT.WRAP);
		titleLabel.setText("Document Title :");
		titleText = new Text(composite, SWT.BORDER);
		titleText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		titleText.setMessage("A title for your document");
		GridData layoutData = new GridData();
		layoutData.widthHint = 150;
		titleText.setLayoutData(layoutData);
		Label titleExplanationLabel = new Label(composite, SWT.WRAP);
		titleExplanationLabel.setText("");
		setFont(font, titleExplanationLabel);

		// Document version
		Label versionLabel = new Label(composite, SWT.WRAP);
		versionLabel.setText("Document Version :");
		versionText = new Text(composite, SWT.BORDER);
		versionText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		versionText.setMessage("1.0");
		Label versionExplanationLabel = new Label(composite, SWT.WRAP);
		versionExplanationLabel.setText("");
		setFont(font, versionExplanationLabel);
		
		// Society
		Label societyLabel = new Label(composite, SWT.WRAP);
		societyLabel.setText("Society :");
		societyName = new Text(composite, SWT.BORDER);
		societyName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		societyName.setMessage("Society Name");
		Label societyExplanationLabel = new Label(composite, SWT.WRAP);
		societyExplanationLabel.setText("");
		setFont(font, societyExplanationLabel);
		
		// Generates improvement suggestions
		Label improveLabel = new Label(composite, SWT.WRAP);
		improveLabel.setText("Suggest improvements");
		improveButton = new Button(composite, SWT.CHECK);
		improveButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				suggestImprovements = improveButton.getSelection();
			}
		});
		Label improveExplanationLabel = new Label(composite, SWT.WRAP);
		improveExplanationLabel.setText("");
		setFont(font, improveExplanationLabel);
		
		setGenerationStyle(DocKind.form);
		genStyleTextualChoice.setSelection(true);
		genStyleTablesChoice.setSelection(false);

		composite.pack();
		// set the composite as the control for this page
		setControl(composite);
	}

	private void setFont(Font font, Label strategyLabel) {
		strategyLabel.setFont(font);
	}

	protected void setGenerationStyle(DocKind value) {
		this.generationStrategy = value;
	}

	@Override
	public IDialogSettings buildDialogSettings() {
		DialogSettings settings = new DialogSettings(SETTINGS_ID);
		settings.put(GENERATION_STRATEGY, generationStrategy.name());
		settings.put(SOCIETY, societyName.getText());
		settings.put(DOCUMENT_TITLE, titleText.getText());
		settings.put(DOCUMENT_VERSION, versionText.getText());
		settings.put(SUGGEST_IMPROVEMENTS, suggestImprovements);
		return settings;
	}

	@Override
	public void loadDialogSettings(IDialogSettings pSettings) {
		if (pSettings != null) {
			// strategy
			String strategy = pSettings.get(GENERATION_STRATEGY);
			DocKind kind = DocKind.form;
			if (strategy != null && "table".equals(strategy)) {
				kind = DocKind.table;
			}
			setGenerationStyle(kind);
			switch (kind) {
			case form:
				genStyleTextualChoice.setSelection(true);
				genStyleTablesChoice.setSelection(false);
				break;
			case table:
				genStyleTextualChoice.setSelection(false);
				genStyleTablesChoice.setSelection(true);
				break;
			}
			// society
			String name = pSettings.get(SOCIETY);
			if (name == null) {
				name = "";
			}
			societyName.setText(name);
			// title
			String title = pSettings.get(DOCUMENT_TITLE);
			if (title == null) {
				title = "";
			}
			titleText.setText(title);
			// version
			String version = pSettings.get(DOCUMENT_VERSION);
			if (version == null) {
				version = "";
			}
			versionText.setText(version);
			// suggestImprovements
			suggestImprovements = pSettings.getBoolean(SUGGEST_IMPROVEMENTS);
			improveButton.setSelection(suggestImprovements);
		}

	}

	@Override
	public String getSectionId() {
		return SETTINGS_ID;
	}

	protected IPxDocWizard getPxDocWizard() {
		return (IPxDocWizard) getWizard();
	}

	@Override
	public void fillLauncher(PxDocLauncher pLauncher) {
		PxDocLauncherHelper.addProperty(pLauncher, GENERATION_STRATEGY, generationStrategy);
		PxDocLauncherHelper.addProperty(pLauncher, SOCIETY, societyName.getText());
		PxDocLauncherHelper.addProperty(pLauncher, DOCUMENT_TITLE, titleText.getText());
		PxDocLauncherHelper.addProperty(pLauncher, DOCUMENT_VERSION, versionText.getText());
		PxDocLauncherHelper.addProperty(pLauncher, SUGGEST_IMPROVEMENTS, suggestImprovements);
	}

}
