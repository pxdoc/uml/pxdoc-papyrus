package org.pragmaticmodeling.pxdoc.uml.papyrus.gen;
				
import com.google.inject.Binder;
import com.google.inject.Module;
				
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
					
public abstract class AbstractPapyrusGeneratorModule implements Module {
					
	@Override
	public void configure(Binder binder) {
		binder.bind(IPxDocGenerator.class).to(PapyrusGenerator.class);
	}

}