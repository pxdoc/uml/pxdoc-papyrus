package org.pragmaticmodeling.pxdoc.uml.papyrus.gen;

import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Underline;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;
import org.pragmaticmodeling.pxdoc.runtime.papyrus.PapyrusDiagramsProvider;
import org.pragmaticmodeling.pxdoc.uml.common.lib.IUMLDescriptionProvider;
import org.pragmaticmodeling.pxdoc.uml.common.lib.UMLCommon;
import org.pragmaticmodeling.pxdoc.uml.common.lib.UMLDescriptionProvider;
import org.pragmaticmodeling.pxdoc.uml402.lib.UML402Delegate;

@SuppressWarnings("all")
public class PapyrusGenerator extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public UMLCommon _UMLCommon = (UMLCommon)getGenerator().getModule(UMLCommon.class);
  
  public void filterUmlFeatures() {
    _UMLCommon.filterUmlFeatures();
  }
  
  public List<Property> getAttributeProperties(final Classifier pClassifier) {
    return _UMLCommon.getAttributeProperties(pClassifier);
  }
  
  public List<Constraint> getConstraints(final EObject element) {
    return _UMLCommon.getConstraints(element);
  }
  
  public String getLower(final MultiplicityElement element) {
    return _UMLCommon.getLower(element);
  }
  
  public List<Property> getRoleProperties(final Classifier pClassifier) {
    return _UMLCommon.getRoleProperties(pClassifier);
  }
  
  public IUMLDescriptionProvider getUmlDescriptionProvider() {
    return _UMLCommon.getUmlDescriptionProvider();
  }
  
  public String getUpper(final MultiplicityElement pElement) {
    return _UMLCommon.getUpper(pElement);
  }
  
  public String multiplicity(final MultiplicityElement me) {
    return _UMLCommon.multiplicity(me);
  }
  
  public void setUmlDescriptionProvider(final IUMLDescriptionProvider provider) {
    _UMLCommon.setUmlDescriptionProvider(provider);
  }
  
  public PapyrusGenerator() {
    super();
  }
  
  public void generate() throws PxDocGenerationException {
    org.eclipse.uml2.uml.NamedElement model = (org.eclipse.uml2.uml.NamedElement)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting PapyrusGenerator generation...");
    	main(null, model);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: PapyrusGenerator generation...");
    }
  }
  
  public List<String> getStylesToBind() {
    if (_stylesToBind == null) {
    	_stylesToBind = new ArrayList<String>();
    	_stylesToBind.add("Emphasis");					
    	_stylesToBind.add("BulletList");					
    	_stylesToBind.add("Title");					
    	_stylesToBind.add("BodyText");					
    	_stylesToBind.add("Normal");					
    	_stylesToBind.add("SubTitle");					
    	_stylesToBind.add("Figure");					
    }
    return _stylesToBind;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * Entry point.
   */
  public void main(final ContainerElement parent, final NamedElement model) throws PxDocGenerationException {
    String _xifexpression = null;
    Object _property = this.getProperty("title");
    boolean _tripleEquals = (_property == null);
    if (_tripleEquals) {
      String _text = this.getText(model);
      _xifexpression = ("Specification of " + _text);
    } else {
      Object _property_1 = this.getProperty("title");
      _xifexpression = ((String) _property_1);
    }
    String specTitle = _xifexpression;
    Object _property_2 = this.getProperty("society");
    String societyName = ((String) _property_2);
    Object _property_3 = this.getProperty("version");
    String version = ((String) _property_3);
    Object _property_4 = this.getProperty("suggestImprovements");
    Boolean suggest = ((Boolean) _property_4);
    UML402Delegate _uML402Delegate = new UML402Delegate();
    UMLDescriptionProvider _uMLDescriptionProvider = new UMLDescriptionProvider(this, _uML402Delegate);
    this.setDescriptionProvider(_uMLDescriptionProvider);
    this.filterUmlFeatures();
    PapyrusDiagramsProvider _papyrusDiagramsProvider = new PapyrusDiagramsProvider();
    this.setDiagramProvider(_papyrusDiagramsProvider);
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, getStylesheetName(), null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    setDocumentVariable(lObj0, "version", version);
    setDocumentProperty(lObj0, "title", specTitle);
    setDocumentProperty(lObj0, "Company", societyName);
    addDocument(lObj0);
    {
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      StyleApplication lObj1 = createStyleApplication(lObj0, false, getBindedStyle("Title"), null, null, null);
      createText(lObj1, specTitle);
      createParagraphBreak(lObj0);
      StyleApplication lObj2 = createStyleApplication(lObj0, false, getBindedStyle("Normal"), AlignmentType.CENTER, null, null);
      {
        createImage(lObj2, "images/companyLogo.png", null, null, null, null, null);
        createParagraphBreak(lObj2);
        Italic lObj3 = createItalic(lObj2);
        createText(lObj3, societyName);
        createParagraphBreak(lObj2);
        createParagraphBreak(lObj2);
        Underline lObj4 = createUnderline(lObj2, UnderlineType.SINGLE);
        {
          createText(lObj4, "Version: ");
          createText(lObj4, version);
        }
      }
      createPageBreak(lObj0);
      createToc(lObj0, "Table of Content", "", "Title");
      createParagraphBreak(lObj0);
      createPageBreak(lObj0);
      this.setSuggestImprovements((suggest).booleanValue());
      _UMLCommon.description(lObj0, model, 0);
    }
  }
}
