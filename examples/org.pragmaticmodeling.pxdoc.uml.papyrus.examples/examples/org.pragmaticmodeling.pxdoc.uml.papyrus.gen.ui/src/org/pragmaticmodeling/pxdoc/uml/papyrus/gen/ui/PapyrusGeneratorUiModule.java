package org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;

public class PapyrusGeneratorUiModule extends AbstractPapyrusGeneratorUiModule {
		   
	public PapyrusGeneratorUiModule(AbstractPxUiPlugin plugin) {
		super(plugin);
	}
	
}
	
