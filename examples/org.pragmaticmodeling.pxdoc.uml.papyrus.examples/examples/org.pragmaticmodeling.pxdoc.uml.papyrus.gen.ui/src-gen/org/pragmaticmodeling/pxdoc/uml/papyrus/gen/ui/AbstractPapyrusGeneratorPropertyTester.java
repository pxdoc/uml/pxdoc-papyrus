package org.pragmaticmodeling.pxdoc.uml.papyrus.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.papyrus.AbstractPapyrusPropertyTester;
import org.eclipse.uml2.uml.NamedElement;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractPapyrusGeneratorPropertyTester extends AbstractPapyrusPropertyTester {

	public AbstractPapyrusGeneratorPropertyTester() {
		super(NamedElement.class);
	}
}

