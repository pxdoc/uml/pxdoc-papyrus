package org.pragmaticmodeling.pxdoc.enablers.papyrus;

import com.google.inject.Injector;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGenerator;
import fr.pragmaticmodeling.pxdoc.generator.RuntimeProject;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUiPrj;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxdoc.enablers.papyrus.IPapyrusInputContext;
import org.pragmaticmodeling.pxgen.runtime.AbstractGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public class PapyrusInput extends AbstractGeneratorFragment implements IPapyrusInputContext {
  public PapyrusInput() {
    super();
  }
  
  public IEclipseUiContext getParentContext() {
    return (IEclipseUiContext)getParentFragment();
  }
  
  private String selectionClassFqn;
  
  private String initSelectionClassFqn() {
    return "org.eclipse.emf.ecore.EObject";
  }
  
  @PxGenParameter
  public String getSelectionClassFqn() {
    if (selectionClassFqn == null) {
    	selectionClassFqn = initSelectionClassFqn();	
    }
    return selectionClassFqn;
  }
  
  @PxGenParameter
  public void setSelectionClassFqn(final String selectionClassFqn) {
    this.selectionClassFqn = selectionClassFqn;
  }
  
  private String label;
  
  private String initLabel() {
    String _generatorClassName = this.getGeneratorClassName();
    String _plus = ("Generate " + _generatorClassName);
    return _plus;
  }
  
  @PxGenParameter
  public String getLabel() {
    if (label == null) {
    	label = initLabel();	
    }
    return label;
  }
  
  @PxGenParameter
  public void setLabel(final String label) {
    this.label = label;
  }
  
  private String commandId;
  
  private String initCommandId() {
    String _basePackage = this.getBasePackage();
    String _plus = (_basePackage + ".ui.commands.");
    String _lowerCase = this.getGeneratorClassName().toLowerCase();
    String _plus_1 = (_plus + _lowerCase);
    return _plus_1;
  }
  
  @PxGenParameter
  public String getCommandId() {
    if (commandId == null) {
    	commandId = initCommandId();	
    }
    return commandId;
  }
  
  @PxGenParameter
  public void setCommandId(final String commandId) {
    this.commandId = commandId;
  }
  
  @PxGenParameter
  public void setPluginEntries(final List<String> pluginEntries) {
    getParentContext().setPluginEntries(pluginEntries);
  }
  
  @PxGenParameter
  public List<String> getPluginEntries() {
    return getParentContext().getPluginEntries();
  }
  
  @PxGenParameter
  public void setExecutableExtensionFactoryClassName(final String executableExtensionFactoryClassName) {
    getParentContext().setExecutableExtensionFactoryClassName(executableExtensionFactoryClassName);
  }
  
  @PxGenParameter
  public String getExecutableExtensionFactoryClassName() {
    return getParentContext().getExecutableExtensionFactoryClassName();
  }
  
  @PxGenParameter
  public void setSelectionMode(final String selectionMode) {
    getParentContext().setSelectionMode(selectionMode);
  }
  
  @PxGenParameter
  public String getSelectionMode() {
    return getParentContext().getSelectionMode();
  }
  
  @PxGenParameter
  public void setBaseCommandClass(final String baseCommandClass) {
    getParentContext().setBaseCommandClass(baseCommandClass);
  }
  
  @PxGenParameter
  public String getBaseCommandClass() {
    return getParentContext().getBaseCommandClass();
  }
  
  @PxGenParameter
  public void setCommandClassFqn(final String commandClassFqn) {
    getParentContext().setCommandClassFqn(commandClassFqn);
  }
  
  @PxGenParameter
  public String getCommandClassFqn() {
    return getParentContext().getCommandClassFqn();
  }
  
  @PxGenParameter
  public void setAbstractCommandConstructors(final String abstractCommandConstructors) {
    getParentContext().setAbstractCommandConstructors(abstractCommandConstructors);
  }
  
  @PxGenParameter
  public String getAbstractCommandConstructors() {
    return getParentContext().getAbstractCommandConstructors();
  }
  
  @PxGenParameter
  public void setAbstractCommandMethods(final String abstractCommandMethods) {
    getParentContext().setAbstractCommandMethods(abstractCommandMethods);
  }
  
  @PxGenParameter
  public String getAbstractCommandMethods() {
    return getParentContext().getAbstractCommandMethods();
  }
  
  @PxGenParameter
  public void setCommandConstructors(final String commandConstructors) {
    getParentContext().setCommandConstructors(commandConstructors);
  }
  
  @PxGenParameter
  public String getCommandConstructors() {
    return getParentContext().getCommandConstructors();
  }
  
  @PxGenParameter
  public void setMenuLabel(final String menuLabel) {
    getParentContext().setMenuLabel(menuLabel);
  }
  
  @PxGenParameter
  public String getMenuLabel() {
    return getParentContext().getMenuLabel();
  }
  
  @PxGenParameter
  public void setPropertyTesterFqn(final String propertyTesterFqn) {
    getParentContext().setPropertyTesterFqn(propertyTesterFqn);
  }
  
  @PxGenParameter
  public String getPropertyTesterFqn() {
    return getParentContext().getPropertyTesterFqn();
  }
  
  @PxGenParameter
  public void setBaseModelProvider(final String baseModelProvider) {
    getParentContext().setBaseModelProvider(baseModelProvider);
  }
  
  @PxGenParameter
  public String getBaseModelProvider() {
    return getParentContext().getBaseModelProvider();
  }
  
  @PxGenParameter
  public void setAbstractCommandClassBody(final String abstractCommandClassBody) {
    getParentContext().setAbstractCommandClassBody(abstractCommandClassBody);
  }
  
  public String getCommandObject() {
    return getParentContext().getCommandObject();
  }
  
  public JvmTypeReference findJvmRef(final String fqn) {
    return getParentContext().findJvmRef(fqn);
  }
  
  public String getAbstractCommandClassBody() {
    return getParentContext().getAbstractCommandClassBody();
  }
  
  public EclipseUiPrj getEclipseUiPrj() {
    return getParentContext().getEclipseUiPrj();
  }
  
  public String simpleName(final String qualifiedName) {
    return getParentContext().simpleName(qualifiedName);
  }
  
  public String javaPackage(final String qualifiedName) {
    return getParentContext().javaPackage(qualifiedName);
  }
  
  public String generateExtensions() {
    return getParentContext().generateExtensions();
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxDocUri(final String pxDocUri) {
    getParentContext().setPxDocUri(pxDocUri);
  }
  
  @PxGenParameter
  public String getPxDocUri() {
    return getParentContext().getPxDocUri();
  }
  
  @PxGenParameter
  public void setPxDocGenerator(final PxDocGenerator pxDocGenerator) {
    getParentContext().setPxDocGenerator(pxDocGenerator);
  }
  
  @PxGenParameter
  public void setStylesheet(final File stylesheet) {
    getParentContext().setStylesheet(stylesheet);
  }
  
  @PxGenParameter
  public File getStylesheet() {
    return getParentContext().getStylesheet();
  }
  
  @PxGenParameter
  public void setGeneratorClassName(final String generatorClassName) {
    getParentContext().setGeneratorClassName(generatorClassName);
  }
  
  @PxGenParameter
  public String getGeneratorClassName() {
    return getParentContext().getGeneratorClassName();
  }
  
  @PxGenParameter
  public void setLogger(final Logger logger) {
    getParentContext().setLogger(logger);
  }
  
  @PxGenParameter
  public Logger getLogger() {
    return getParentContext().getLogger();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  @PxGenParameter
  public void setTypeReferences(final TypeReferences typeReferences) {
    getParentContext().setTypeReferences(typeReferences);
  }
  
  @PxGenParameter
  public TypeReferences getTypeReferences() {
    return getParentContext().getTypeReferences();
  }
  
  public Object addDevelopmentTimeBundles(final IGeneratorFragment f) {
    return getParentContext().addDevelopmentTimeBundles(f);
  }
  
  public String getProjectName(final Class<?> clazz) {
    return getParentContext().getProjectName(clazz);
  }
  
  public RuntimeProject getRuntimeProject() {
    return getParentContext().getRuntimeProject();
  }
  
  public String getPxDocGeneratorClassName() {
    return getParentContext().getPxDocGeneratorClassName();
  }
  
  public PxDocGenerator getPxDocGenerator() {
    return getParentContext().getPxDocGenerator();
  }
  
  public PxDocGenerator loadGenerator() {
    return getParentContext().loadGenerator();
  }
  
  public Void setModelObject(final String object) {
    return getParentContext().setModelObject(object);
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
    if (this.selectionClassFqn == null) {
    	this.selectionClassFqn = initSelectionClassFqn();
    }
    if (this.label == null) {
    	this.label = initLabel();
    }
    if (this.commandId == null) {
    	this.commandId = initCommandId();
    }
  }
  
  @Override
  public void doGenerate() {
    {
      this.setModelObject("org.eclipse.uml2.uml.Element");
      this.setBaseModelProvider("org.pragmaticmodeling.pxdoc.runtime.ui.papyrus.DefaultPapyrusModelProvider");
      this.setPropertyTesterFqn("org.pragmaticmodeling.pxdoc.runtime.ui.papyrus.AbstractPapyrusPropertyTester");
      List<String> _requiredBundles = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles.add("org.pragmaticmodeling.pxdoc.runtime.ui.eclipse");
      List<String> _requiredBundles_1 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_1.add("org.eclipse.papyrus.emf.facet.custom.metamodel");
      List<String> _requiredBundles_2 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_2.add("org.pragmaticmodeling.pxdoc.emf.lib");
      List<String> _requiredBundles_3 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_3.add("org.pragmaticmodeling.pxdoc.runtime.ui.papyrus");
      List<String> _requiredBundles_4 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_4.add("org.pragmaticmodeling.pxdoc.common.lib");
      List<String> _requiredBundles_5 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_5.add("org.pragmaticmodeling.pxdoc.diagrams.lib");
      List<String> _requiredBundles_6 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_6.add("org.pragmaticmodeling.pxdoc.plugins.html2pxdoc");
      List<String> _requiredBundles_7 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_7.add("org.pragmaticmodeling.pxdoc.emf.lib");
      List<String> _requiredBundles_8 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_8.add("org.pragmaticmodeling.pxdoc.images.lib");
      List<String> _requiredBundles_9 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_9.add("org.pragmaticmodeling.pxdoc.runtime.papyrus");
      List<String> _requiredBundles_10 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_10.add("org.eclipse.uml2.uml;visibility:=reexport");
      List<String> _requiredBundles_11 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_11.add("org.pragmaticmodeling.pxdoc.uml.common.lib");
      List<String> _requiredBundles_12 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_12.add("org.pragmaticmodeling.pxdoc.uml402.lib");
      List<String> _developmentTimeBundles = this.getRuntimeProject().getDevelopmentTimeBundles();
      _developmentTimeBundles.add("org.eclipse.uml2.uml");
    }
  }
}
