package org.pragmaticmodeling.pxdoc.enablers.papyrus;

import java.util.List;

import fr.pragmaticmodeling.pxdoc.dsl.ui.enablers.AbstractPlatformEnabler;

public class PapyrusEnabler extends AbstractPlatformEnabler {

	public PapyrusEnabler() {
		super();
	}

	@Override
	public String getJavaClass() {
		return "org.pragmaticmodeling.pxdoc.enablers.papyrus.PapyrusInput";
	}

	@Override
	public List<String> getDevelopmentTimesBundles() {
		List<String> result = super.getDevelopmentTimesBundles();
		result.add("org.pragmaticmodeling.pxdoc.enablers.papyrus");
		return result;
	}

	@Override
	public String getModelObject() {
		return "org.eclipse.uml2.uml.Element";
	}

	@Override
	public List<String> getRequiredBundles(boolean withPxdocLibs) {
		List<String> result = super.getRequiredBundles(withPxdocLibs);
		result.add("org.pragmaticmodeling.pxdoc.runtime.papyrus");
		result.add("org.eclipse.uml2.uml;visibility:=reexport");
		result.add("org.pragmaticmodeling.pxdoc.plugins.diagrams");
		result.add("org.pragmaticmodeling.pxdoc.plugins.html2pxdoc");
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.diagrams.lib");
			result.add("org.pragmaticmodeling.pxdoc.common.lib");
			result.add("org.pragmaticmodeling.pxdoc.emf.lib");
			result.add("org.pragmaticmodeling.pxdoc.uml.common.lib");
			result.add("org.pragmaticmodeling.pxdoc.uml402.lib");
		}
		return result;
	}

	@Override
	public String getRootTemplateDeclarations() {
		String result = "// Commons lib configuration : set a description provider able to get information about object description\n";
		result += "descriptionProvider = new UMLDescriptionProvider(this, new UML402Delegate)\n";
		result += "// Diagrams lib configuration : Papyrus Diagrams Provider injected in DiagramServices\n";
		result += "diagramProvider = new PapyrusDiagramsProvider\n";
		result += "// UMLCommon basic filtering\n";
		result += "filterUmlFeatures\n";
		return result;
	}

	@Override
	public List<String> getWithModules() {
		List<String> result = super.getWithModules();
		result.add("org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices");
		result.add("org.pragmaticmodeling.pxdoc.common.lib.CommonServices");
		result.add("org.pragmaticmodeling.pxdoc.emf.lib.EmfServices");
		result.add("org.pragmaticmodeling.pxdoc.uml.common.lib.UMLCommon");
		return result;
	}

	@Override
	public List<String> getPxdocImports(boolean withPxdocLibs) {
		List<String> result = super.getPxdocImports(withPxdocLibs);
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.uml.common.lib.UMLDescriptionProvider");
			result.add("org.pragmaticmodeling.pxdoc.runtime.papyrus.PapyrusDiagramsProvider");
			result.add("org.pragmaticmodeling.pxdoc.uml402.lib.UML402Delegate");
		}
		return result;
	}

}
